#include "all.h"

//------< データ >---------------------------------------------------------------
// 2D画像ロードデータ
LoadTexture loadTexture[] = {
    { TEXNO::TESTIMAGE1,               L"./Data/Images/testimage1.png",            1U },
    { TEXNO::TESTIMAGE2,               L"./Data/Images/testimage2.png",            1U },

   { -1, nullptr }	// 終了フラグ
};

//******************************************************************************
