#include "all.h"
#include "scene_title.h"


//-----コンストラクタ-------------------------------------------------------------------------------------------------
SceneTitle::SceneTitle()
	: Scene() {}

//-----デストラクタ-------------------------------------------------------------------------------------------------
SceneTitle::~SceneTitle()
{
	// テクスチャの解放
	texture::releaseAll();
}

//-----更新処理-------------------------------------------------------------------------------------------------
void SceneTitle::update()
{
	switch (state)
	{
	case 0:
		//////// 初期設定 ////////


		// テクスチャの読み込み
		texture::load(loadTexture);

		++state;
		/*fallthrough*/

	case 1:
		/////// パラメーターの設定 ////////

		GameLib::setBlendMode(Blender::BS_ALPHA);

		++state;
		/*fallthrough*/

	case 2:
		//////// 通常時 ////////
		if (TRG(0) & PAD_START)  setScene(SCENE_GAME);


		break;
	}

#ifdef USE_IMGUI
	ImGui::Begin("ImGUI");



	ImGui::End();
#endif

	++timer;
}

//-----描画処理-------------------------------------------------------------------------------------------------
void SceneTitle::draw()
{
	GameLib::clear(0.6f, 0.2f, 0.4f);
	//文字の表示
	text_out(4, "PROTO TYPE", 300, 100, 4, 3, 1, 0, 0);

	text_out(4, "Push Enter Key", 300, 500, 2, 2, 1, 0, 0);
}