#ifndef ALL_H
#define ALL_H

//マクロ
#define ToRadian( x )   DirectX::XMConvertToRadians( x )
#define ToDegree( x )   DirectX::XMConvertToDegrees( x )

// システムやライブラリのヘッダーファイル
#include "../GameLib/game_lib.h"

// 共通で使うようなヘッダー
#include "main.h"
#include "common.h"
#include "sprite_data.h"

// 親クラスがあるヘッダー
#include "Scene.h"

//******************************************************************************

#ifdef USE_IMGUI
#include "../imgui/imgui.h"
#include "../imgui/imgui_internal.h"
#include "../imgui/imgui_impl_dx11.h"
#include "../imgui/imgui_impl_win32.h"
#endif

// メモリーリークを調べる
#include<crtdbg.h>
#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//namespace
using namespace std;
using namespace GameLib;
using namespace input;
#endif // !ALL_H
