#include "system.h"
void draw_texture(int texture_index, VECTOR2 position, VECTOR2 scale, VECTOR2 cutpos_start,
	VECTOR2 cutsize, VECTOR2 center, int angle, VECTOR4 color)
{
	texture::begin(texture_index);
	texture::draw(texture_index, position.x, position.y, scale.x, scale.y,
		cutpos_start.x, cutpos_start.y, cutsize.x, cutsize.y, center.x, center.y, angle, color.x, color.y, color.z, color.w);
	texture::end(texture_index);

}

VECTOR2 get_cursolpos()
{
	POINT point;
	GetCursorPos(&point);

	return VECTOR2 { (float)point.x, (float)point.y };
}

void draw_tites(VECTOR2 camera_pos)
{
	for (int h = 0; h < TILEMAX_H; h++)
	{
		for (int w = 0; w < TILEMAX_W; w++)
		{
			{
				switch (map_tile[w][h])
				{
				case 1:
					draw_texture(TESTTILE1, VECTOR2{ w * 64.0f + camera_pos.x,h * 64.0f + camera_pos.y }, VECTOR2{ 1,1 }, VECTOR2{ 0,0 },
						VECTOR2{ 64, 64 }, VECTOR2{ 0, 0 }, 0, VECTOR4{ 1,1,1,1 });
					break;
				case 2:
					draw_texture(TESTTILE2, VECTOR2{ w * 64.0f + camera_pos.x,h * 64.0f + camera_pos.y }, VECTOR2{ 1,1 }, VECTOR2{ 0,0 },
						VECTOR2{ 64, 64 }, VECTOR2{ 0, 0 }, 0, VECTOR4{ 1,1,1,1 });
				default:
					break;
				}
			}
		}
	}
}