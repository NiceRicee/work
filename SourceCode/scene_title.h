#ifndef	SCENE_TITLE_H
#define	SCENE_TITLE_H

class SceneTitle : public Scene
{
private:
    //--------<変数>--------

public:
    //--------<コンストラクタ/関数等>--------
    SceneTitle();
    void update() override;
    void draw() override;
    ~SceneTitle();
};

#endif//SCENE_TITLE_H