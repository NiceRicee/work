#ifndef  SCENE_GAME_H
#define	 SCENE_GAME_H

class SceneGame : public Scene
{
private:
    //--------<変数>--------

public:
    //--------<コンストラクタ/関数等>--------
    SceneGame();
    void update() override;
    void draw() override;
    ~SceneGame();
};

#endif //  SCENE_GAME_H