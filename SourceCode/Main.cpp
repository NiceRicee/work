//�@必要なファイルをインクルード
#include "all.h"
#include "scene_game.h"
#include "scene_title.h"
#include <time.h>
// 変数
int nextScene       = SCENE_TITLE;
bool changeSceneFlg = true;

// 実体宣言
unique_ptr<Scene> sceneGame(nullptr);

//******************************************************************************
//
//
//		WinMain関数（エントリポイント）
//
//
//******************************************************************************

//�AWinMain関数を記述する
int APIENTRY wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	// 乱数のリセット
	srand((unsigned int)time(NULL));
	//�Cゲームライブラリの初期設定
	GameLib::init(L"プロト", SCREEN_W, SCREEN_H);

	// オーディオの初期設定


	//�Eゲームループ
	while (GameLib::gameLoop(true))
	{
		if (changeSceneFlg)
		{
			switch (nextScene)
			{
			case SCENE_TITLE:  sceneGame.reset(new SceneTitle());  break;
			case SCENE_GAME:   sceneGame.reset(new SceneGame());   break;
			}
			sceneGame->init();
			changeSceneFlg = false;
		}

		//入力を更新する
		input::update();
		// 音楽の更新処理


#ifdef USE_IMGUI
		ImGui_ImplDX11_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();
#endif

		// 更新処理
		sceneGame->update();
		// 描画処理
		sceneGame->draw();

#ifdef USE_IMGUI
		ImGui::Render();
		ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif

		//デバッグ用文字列の表示
		debug::display(1, 0, 1, 2, 2);

		//�F画面を描画する
		GameLib::present(1, 0);
	}

	// オーディオの終了処理


	//シーンの解放(これがないとsceneGameはグローバル変数のためメモリーリークが生じる)
	sceneGame.reset();


	//�Dゲームライブラリの終了処理
	GameLib::uninit();

	//�B戻り値は0でよい
	return 0;
}

void setScene(int scene)
{
	nextScene      = scene;
	changeSceneFlg = true;
}
