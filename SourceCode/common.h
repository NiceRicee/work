#ifndef COMMON_H
#define COMMON_H

//シーンのラベル
enum
{
	SCENE_TITLE,
	SCENE_GAME,
};

//画面の大きさ
#define SCREEN_W   1280
#define SCREEN_H    720

#endif // COMMON_H
