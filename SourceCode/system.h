#pragma once
#include "all.h"

void draw_texture(int texture_index, VECTOR2 position, VECTOR2 scale, VECTOR2 cutpos_start,
	VECTOR2 cutsize, VECTOR2 center, int angle, VECTOR4 color);

VECTOR2 get_cursolpos();

void draw_tites(VECTOR2 camera_pos);