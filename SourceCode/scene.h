#pragma once


class Scene
{
protected:
    //--------<定数>--------


    //--------<変数>--------
    int		state;
    int     timer;

public:
    //--------<コンストラクタ/関数等>--------
    Scene();
    virtual void init() {}
    virtual void update() = 0;
    virtual void draw() = 0;
};